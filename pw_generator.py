#!/usr/bin/env python
# coding: utf-8

# In[2]:


import random

lower = "abcdefghijklmnopqrstuvwxyz"
upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
numbers = "0123456789"
symbols = "[]{}()*@%&$!"

all = lower + upper + numbers + symbols
length = 15
password = "".join(random.sample(all, length))
print(password)


# In[ ]:




